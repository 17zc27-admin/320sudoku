#include "my_pushbutton.h"
#include <QtDebug>
//My_pushButton::My_pushButton(QWidget *parent) : QPushButton(parent)
//{

//}
My_pushButton::My_pushButton(QString normalImg, QString pressImg){
    this->normalImgPath = normalImg;
    this->pressImgPath = pressImg;

    QPixmap pix;
    bool ret = pix.load(normalImg);
    if(!ret){
        qDebug() <<"Fail to load the picture";
        return;
    }

    this->setFixedSize(pix.width(), pix.height());
    this->setStyleSheet("QPushButton{border:0px;}");

    this->setIcon(pix);
    this->setIconSize(QSize(pix.width(), pix.height()));

}
