#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "dialog.h"
//#include "my_pushbutton.h"
#include <QLabel>
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->e_btn->hide();
    ui->m_btn->hide();
    ui->h_btn->hide();
    ui->cancel_btn->hide();
    QPixmap logo(":/sudokuPhoto/logo.png");
    ui->label->setPixmap(logo.scaled(ui->label->width(),ui->label->height(),Qt::KeepAspectRatio));
     QPixmap boardPic(":/sudokuPhoto/boardPic.png");
    ui->label_2->setPixmap(boardPic.scaled(ui->label_2->width(),ui->label_2->height(),Qt::KeepAspectRatio));

    //set window size
    this->setFixedSize(1800,1300);
    //set logo
    setWindowIcon(QIcon(":/sudokuPhoto/logo.png"));
    //set window name
    setWindowTitle("Sudoku Solver");
    connect(ui->actionQuit,&QAction::triggered,[=](){
        this->close();
    });



   // My_pushButton * playBtn = new My_pushButton;



}

MainWindow::~MainWindow()
{
    delete ui;
}






void MainWindow::on_settings_btn_clicked()
{
    Dialog *settingsScreen =new Dialog;

    this->hide();
    settingsScreen->show();
//    settingsScreen->setModal(true);
//    settingsScreen->exec();
}

void MainWindow::on_pm_btn_clicked()
{
    ui->ptr_label->setText("------>>");
    ui->e_btn->show();
    ui->m_btn->show();
    ui->h_btn->show();
    ui->cancel_btn->show();


}

void MainWindow::on_cancel_btn_clicked()
{
    ui->ptr_label->setText("");
    ui->e_btn->hide();
    ui->m_btn->hide();
    ui->h_btn->hide();
    ui->cancel_btn->hide();
}
