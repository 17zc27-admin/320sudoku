#include "dialog.h"
#include "ui_dialog.h"
#include "mainwindow.h"

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    this->setFixedSize(1800,1300);
    QPixmap logo(":/sudokuPhoto/logo.png");
    ui->label->setPixmap(logo.scaled(ui->label->width(),ui->label->height(),Qt::KeepAspectRatio));
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::on_back_btn_clicked()
{
    this->hide();
     MainWindow *mainwindow =new MainWindow;
    mainwindow->show();
}
